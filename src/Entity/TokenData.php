<?php

declare(strict_types=1);

namespace webnode\oauth2\Entity;

use DateTimeImmutable;

/**
 * Object representing "content" of token
 */
final class TokenData
{
	private readonly DateTimeImmutable $expiresAt;
	
	
	/**
	 * @param string[] $scopes
	 * @param mixed[] $sessionData
	 */
	public function __construct(
		private readonly bool $approved,
		private readonly ?string $userIdentifier,
		private readonly ?string $partnerIdentifier,
		private readonly array $scopes,
		private readonly array $sessionData,
		int $expiresAt
	) {
		$this->expiresAt = (new DateTimeImmutable())->setTimestamp($expiresAt);
	}
	
	
	public function isApproved(): bool
	{
		return $this->approved;
	}
	
	public function getUserIdentifier(): ?string
	{
		return $this->userIdentifier;
	}

	public function getPartnerIdentifier(): ?string
	{
		return $this->partnerIdentifier;
	}
	
	
	/**
	 * @return string[]
	 */
	public function getScopes(): array
	{
		return $this->scopes;
	}
	
	
	/**
	 * @return mixed[]
	 */
	public function getSessionData(): array
	{
		return $this->sessionData;
	}
	
	
	public function getExpiresAt(): DateTimeImmutable
	{
		return $this->expiresAt;
	}
}
