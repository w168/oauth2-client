<?php
declare(strict_types=1);

namespace webnode\oauth2\Entity;

final class Client
{
	/**
	 * @param string[] $redirectUris
	 */
	public function __construct(
		private readonly string $clientId,
		private readonly string $partnerIdentifier,
		private readonly array $redirectUris
	) {
	}

	/**
	 * @return string
	 */
	public function getClientId(): string
	{
		return $this->clientId;
	}

	public function getPartnerIdentifier(): string
	{
		return $this->partnerIdentifier;
	}

	/**
	 * @return string[]
	 */
	public function getRedirectUris(): array
	{
		return $this->redirectUris;
	}
}
