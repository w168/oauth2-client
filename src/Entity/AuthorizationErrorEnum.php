<?php

declare(strict_types=1);

namespace webnode\oauth2\Entity;

abstract class AuthorizationErrorEnum
{
	// oauth2 errors
	const INVALID_REQUEST      = 'invalidAuthorizationRequest';
	const NO_ACCESS_TOKEN      = 'noAccessTokenAssigned';
	const INVALID_CLIENT       = 'invalidClient';
	const INVALID_ACCESS_TOKEN = 'invalidAccessToken';
	const INVALID_AUTH_CODE    = 'invalidAuthorizationCode';
	const ACCESS_DENIED        = 'accessDenied';
	const UNKNOWN_OAUTH2_SERVER_SESSION = 'unknownOAuth2ServerSession';
	
	// translations for APP
	const APP_ACCESS_DENIED = 'accessDenied';
	const APP_UNAUTHORIZED = 'unauthorized';
	const APP_INVALID_CLIENT = 'invalidClient';
	
	// unexpected exception
	const UNKNOWN_ERROR = 'unknown';
}
