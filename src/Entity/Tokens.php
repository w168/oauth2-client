<?php

declare(strict_types=1);

namespace webnode\oauth2\Entity;

/**
 * Object representing response from auth serveru passing access and refresh token
 */
final class Tokens
{
	private string $access;
	
	private ?string $refresh = null;
	
	private int $expiresIn;
	
	public function getAccess(): ?string
	{
		return $this->access;
	}
	
	public function setAccess(string $access): Tokens
	{
		$this->access = $access;
		
		return $this;
	}
	
	public function getRefresh(): ?string
	{
		return $this->refresh;
	}
	
	
	public function setRefresh(?string $refresh): Tokens
	{
		$this->refresh = $refresh;
		
		return $this;
	}
	
	
	public function getExpiresIn(): int
	{
		return $this->expiresIn;
	}
	
	
	public function setExpiresIn(int $expiresIn): Tokens
	{
		$this->expiresIn = $expiresIn;
		
		return $this;
	}
	
	
	/**
	 * @return Array<string, string|int>
	 *
	 */
	public function toArray(): array
	{
		$data  = [
			'token_type' => 'Bearer',
			'expires_in' => $this->expiresIn,
			'access_token' => $this->access,
		];
		if ($this->refresh)
		{
			$data['refresh_token'] = $this->refresh;
		}
		
		return $data;
	}
}
