<?php

declare(strict_types=1);

namespace webnode\oauth2\Entity;

use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token\Plain;
use webnode\oauth2\Exception\UnauthorizedException;

final class State
{
	const ACTION_AUTHORIZE_OAUTH2_SERVER = 1;
	const ACTION_AUTHORIZE_SELF = 2;
	

	private int $action;
	
	/**
	 * Pokud je nějaká akce na více pokusů, tak označení kolikátý je toto pokus
	 */
	private int $attempts = 0;
	
	private ?string $redirectUri;
	

	public function getAction(): int
	{
		return $this->action;
	}
	
	
	public function setAction(int $action): State
	{
		$this->action = $action;
		
		return $this;
	}
	
	public function getAttempts(): int
	{
		return $this->attempts;
	}
	
	
	public function setAttempts(int $attempts): State
	{
		$this->attempts = $attempts;
		
		return $this;
	}
	
	
	public function getRedirectUri(): ?string
	{
		return $this->redirectUri;
	}
	
	
	public function setRedirectUri(?string $redirectUri): State
	{
		$this->redirectUri = $redirectUri;
		
		return $this;
	}
	
	
	public function generateJwtToken(Key $cryptKey): string
	{
		$stateBuilder =  (new Builder(new JoseEncoder(), ChainedFormatter::default()));
		$data = [
			'action' => $this->action,
			'attempts' => $this->attempts,
			'redirectUri' => $this->redirectUri,
		];
		foreach ($data as $key => $value)
		{
			if (!empty($value))
			{
				$stateBuilder = $stateBuilder->withClaim($key, $value);
			}
		}
		return $stateBuilder->getToken(new Sha256(), $cryptKey)->toString();
	}
	
	public static function validFromJwt(string $token, Key $cryptKey): State
	{
		if ($token === '')
		{
			throw new UnauthorizedException('Empty JWT token');
		}

		$token = (new Parser(new JoseEncoder()))->parse($token);

		/** @var Plain $token */
		if (!(new Sha256())->verify($token->signature()->hash(), $token->payload(), $cryptKey))
		{
			throw new UnauthorizedException('Invalid JWT token');
		}
		$claims = $token->claims();
		$redirectUri = $claims->get('redirectUri');
		return (new self())
			->setAction((int)$claims->get('action'))
			->setAttempts((int)$claims->get('attempts', 0))
			->setRedirectUri(empty($redirectUri) ? null : (string)$redirectUri)
			;
	}
}
