<?php

declare(strict_types=1);

namespace webnode\oauth2\Service;

use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Interface for acquiring Access Token
 */
interface AccessInterface
{
	/**
	 * @param string $username
	 * @param string $password
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function passwordGrant(
		string $username,
		string $password,
		array $scope = ['basic'],
		array $sessionData = null
	): Tokens;
	
	
	/**
	 * @param string $code
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function authCodeGrant(string $code, array $scope = ['basic'], array $sessionData = null): Tokens;
	
	
	/**
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function clientCredentialsGrant(array $scope = ['basic'], array $sessionData = null): Tokens;
}
