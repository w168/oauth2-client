<?php

namespace webnode\oauth2\Service;

use webnode\oauth2\Entity\Client;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Representing endpoint for client information
 */
interface ClientServiceInterface
{
	/**
	 * @throws OAuth2Exception
	 */
	public function getCurrentClientInfo(): Client;

	/**
	 * Get client data
	 *
	 * @throws OAuth2Exception
	 */
	public function getClientInfo(string $name, string $password): Client;
}
