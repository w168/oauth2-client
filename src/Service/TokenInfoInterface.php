<?php

namespace webnode\oauth2\Service;

use webnode\oauth2\Entity\TokenData;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Representing endpoint for geting token data
 */
interface TokenInfoInterface
{
	/**
	 * @param string $accessToken
	 * @param string|null $urlPath (path override)
	 * @return TokenData
	 * @throws OAuth2Exception
	 */
	public function getTokenData(string $accessToken, ?string $urlPath = null): TokenData;
}
