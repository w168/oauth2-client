<?php

namespace webnode\oauth2\Service;

use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Representing endpoint for revoking tokens (logout)
 */
interface RevokeInterface
{
	/**
	 * Revokes access and refresh tokens and thus all access to resources connected to them
	 *
	 * @param Tokens $tokens
	 * @param bool $revokeRefreshByAccess
	 * @throws OAuth2Exception
	 */
	public function revokeTokens(Tokens $tokens, bool $revokeRefreshByAccess): void;
	
	
	/**
	 * Revoke Access for all tokens of specific user
	 * @param string $userIdentifier
	 * @throws OAuth2Exception
	 */
	public function revokeUserAccess(string $userIdentifier): void;
}
