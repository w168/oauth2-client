<?php

namespace webnode\oauth2\Service;

use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Representing endpoint for getting status of OAuth2 server
 */
interface StatusInterface
{
	/**
	 * Returns true if OAuth2 API is supposed to work
	 *
	 * @return bool
	 * @throws OAuth2Exception
	 */
	public function isOAuth2ServerOk(): bool;
}
