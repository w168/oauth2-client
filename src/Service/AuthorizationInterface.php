<?php

namespace webnode\oauth2\Service;

use Psr\Http\Message\UriInterface;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Interface for acquiring and using AuthorizationCode
 */
interface AuthorizationInterface
{
	/**
	 * Returns Linked AuthCode.
	 * Any access granted by this code will end, when this AccessToken is expired/revoked
	 *
	 * @param string $accessToken
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getLinkedAuthCode(string $accessToken): string;
	
	
	/**
	 * Returns AuthCode generating accessToken for UserId
	 *
	 * @param string $userIdentifier
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @param int|null $ttl
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getAuthCodeForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string;
	
	
	/**
	 * Returns AuthCode generating accessToken for UserId
	 *
	 * @param string $userIdentifier
	 * @param string $redirectUri (where to send success/error)
	 * @param string|null $appRedirectUri (if filled, instead of redirecting to redirect uri with success,
	 * 										appRedirectUri is destination instead, with state and auth code)
	 * @param string|null $stateRedirectUri (final destination URL)
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @param int|null $ttl
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getAuthorizationUrlForUser(
		string $userIdentifier,
		string $redirectUri,
		?string $appRedirectUri,
		?string $stateRedirectUri,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string;
	
	
	/**
	 * Redirect to this URL will be send to redirectUri with authorization code in query, if brrowser is authorized
	 * Viz AuthorizationInterface::getAuthorizeBrowserUrl
	 *
	 * @param string $redirectUri
	 * @param string $state
	 * @return UriInterface
	 */
	public function getAuthorizeAppUrl(string $redirectUri, string $state = ''): UriInterface;
	
	
	/**
	 * Redirect to this URL to authorize browser with OAuth2 server.
	 * OAuth2 server will then redirect back to redirectUri
	 * Access to unauthorized application (eg. CMS) will be automaticaly granted based on browser authorization
	 *
	 * @param string $code
	 * @param string $redirectUri
	 * @param string $state
	 * @param string|null $authorizeApp
	 * @param string|null $authorityUrl (override url adresy)
	 * @param int|int $ttl
	 * @return UriInterface
	 */
	public function getAuthorizeBrowserUrl(
		string $code,
		string $redirectUri,
		string $state = '',
		?string $authorizeApp = null,
		?string $authorityUrl = null,
		?int $ttl = null
	): UriInterface;
	
	
	/**
	 * Creates single sign out URL
	 * @param string $redirectUri
	 * @return UriInterface
	 *
	 */
	public function getSSOutUrl(string $redirectUri): UriInterface;
}
