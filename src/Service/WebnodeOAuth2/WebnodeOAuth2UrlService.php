<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use Psr\Http\Message\UriInterface;
use webnode\oauth2\Service\AuthorizationInterface;
use webnode\oauth2\Service\OAuth2UrlServiceInterface;

final class WebnodeOAuth2UrlService implements OAuth2UrlServiceInterface
{
	
	public function __construct(
		private readonly AuthorizationInterface $authorizationService
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getAuthorizeAppUrl(string $redirectUri, string $state = ''): UriInterface
	{
		return $this->authorizationService->getAuthorizeAppUrl($redirectUri, $state);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getAuthorizeBrowserUrl(
		string $code,
		string $redirectUri,
		string $state = '',
		?string $authorizeApp = null,
		?string $authorityUrl = null,
		?int $ttl = null
	): UriInterface {
		return $this->authorizationService->getAuthorizeBrowserUrl(
			$code,
			$redirectUri,
			$state,
			$authorizeApp,
			$authorityUrl,
			$ttl
		);
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public function getSSOutUrl(string $redirectUri): UriInterface
	{
		return $this->authorizationService->getSSOutUrl($redirectUri);
	}
}
