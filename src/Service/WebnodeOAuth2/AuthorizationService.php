<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\UriInterface;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\AuthorizationInterface;

/**
 * Interface for acquiring and using AuthorizationCode
 */
final class AuthorizationService implements AuthorizationInterface
{
	private const PATH = '/authorize';

	public function __construct(
		private readonly string $clientId,
		private readonly string $clientSecret,
		private readonly string $baseUri,
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator
	) {
	}


	/**
	 * @inheritDoc
	 */
	public function getLinkedAuthCode(string $accessToken): string
	{
		return $this->getAuthByPost(['headers' => ['Authorization' => "Bearer $accessToken"]]);
	}


	/**
	 * @inheritDoc
	 */
	public function getAuthorizeAppUrl(string $redirectUri, string $state = ''): UriInterface
	{
		$uri = new Uri($this->baseUri . self::PATH);
		return Uri::withQueryValues($uri, [
			'response_type' => 'code',
			'client_id' => $this->clientId,
			'redirect_uri' => $redirectUri,
			'state' => $state,
		]);
	}


	/**
	 * @inheritDoc
	 */
	public function getAuthorizeBrowserUrl(
		string $code,
		string $redirectUri,
		string $state = '',
		?string $authorizeApp = null,
		?string $authorityUrl = null,
		?int $ttl = null
	): UriInterface {
		$authorityUrl = $authorityUrl ?? $this->baseUri . self::PATH;
		$uri = new Uri($authorityUrl);
		$data = [
			'response_type' => 'code',
			'client_id' => $this->clientId,
			'redirect_uri' => $redirectUri,
			'code' => $code,
			'state' => $state,
		];
		if ($ttl !== null)
		{
			$data['ttl'] = (string)$ttl;
		}
		if ($authorityUrl)
		{
			$data['authorize_app'] = $authorizeApp;
		}
		$uri = Uri::withQueryValues($uri, $data);
		return $uri;
	}


	/**
	 * @param mixed[] $options
	 * @param bool $code
	 * @return string
	 */
	private function getAuthByPost(array $options, bool $code = true): string
	{
		try
		{
			$response = $this->httpClient->request('POST', self::PATH, array_merge(
				['http_errors' => false,],
				$options
			));
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}

		$body = $this->responseValidator->getValidJsonResponseData($response);
		$key = $code ? 'code' : 'authorizationUrl';

		if (empty($body[$key]))
		{
			throw new UnexpectedResponseException('Response does not contain code');
		}
		return (string)$body[$key];
	}


	/**
	 * @inheritDoc
	 */
	public function getAuthCodeForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string {
		$data = [
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
			'user_identifier' => $userIdentifier,
			'redirect_uri' => '',
			'scope' => implode(' ', $scope),
		];
		if ($ttl !== null)
		{
			$data['ttl'] = $ttl;
		}
		if ($sessionData)
		{
			$data['session_data'] = json_encode($sessionData);
		}
		return $this->getAuthByPost(['form_params' => $data]);
	}


	/**
	 * @inheritDoc
	 */
	public function getAuthorizationUrlForUser(
		string $userIdentifier,
		string $redirectUri,
		?string $appRedirectUri,
		?string $stateRedirectUri,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string {
		$data = [
			'client_id'     => $this->clientId,
			'client_secret' => $this->clientSecret,
			'user_identifier' => $userIdentifier,
			'redirect_uri' => $redirectUri,
			'scope' => implode(' ', $scope),
		];
		if ($sessionData)
		{
			$data['session_data'] = json_encode($sessionData);
		}
		if ($appRedirectUri)
		{
			$data['app_uri'] = $appRedirectUri;
		}
		if ($stateRedirectUri)
		{
			$data['dest_uri'] = $stateRedirectUri;
		}
		if ($ttl)
		{
			$data['ttl'] = $ttl;
		}

		return $this->getAuthByPost(['form_params' => $data], false);
	}


	/**
	 * @param string $redirectUri
	 * @return UriInterface
	 *
	 */
	public function getSSOutUrl(string $redirectUri): UriInterface
	{
		$uri = new Uri($this->baseUri . self::PATH);
		return Uri::withQueryValues($uri, [
			'response_type' => 'code',
			'client_id' => $this->clientId,
			'redirect_uri' => $redirectUri,
			'logout' => '1',
		]);
	}
}
