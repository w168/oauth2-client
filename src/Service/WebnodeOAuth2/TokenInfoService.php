<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Entity\TokenData;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\TokenInfoInterface;

final class TokenInfoService implements TokenInfoInterface
{
	private const PATH = '/token_info';
	
	
	public function __construct(
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getTokenData(string $accessToken, ?string $urlPath = null): TokenData
	{
		$urlPath = $urlPath ?? self::PATH;
		try
		{
			$response = $this->httpClient->request('GET', $urlPath, [
				'http_errors' => false,
				'headers'     => [
					'Authorization' => "Bearer $accessToken",
				],
			]);
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}
		
		if ($response->getStatusCode() === 401)
		{
			return new TokenData(false, null, null, [], [], 0);
		}
		
		$body = $this->responseValidator->getValidJsonResponseData($response);
		
		if (!isset($body['approved']) || (isset($body['scopes']) && !is_array($body['scopes'])))
		{
			throw new UnexpectedResponseException('Response data are not valid ' . print_r($body, true));
		}
		
		return new TokenData(
			$body['approved'],
			$body['userIdentifier'] ?? null,
			$body['partnerIdentifier'] ?? null,
			$body['scopes'] ?? [],
			$body['sessionData'] ?? [],
			$body['expiresAt'] ?? []
		);
	}
}
