<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\InvalidRequestException;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\ClientServiceInterface;
use webnode\oauth2\Service\RevokeInterface;

final class ClientService implements ClientServiceInterface
{
	private const PATH = '/client';
	
	public function __construct(
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator,
		private ?string $clientId = null,
		private ?string $clientSecret = null
	) {
	}
	public function getCurrentClientInfo(): \webnode\oauth2\Entity\Client
	{
		if (empty($this->clientId) || empty($this->clientSecret))
		{
			throw new InvalidRequestException('Missing credentials');
		}
		return $this->getClientInfo($this->clientId, $this->clientSecret);
	}
	
	/**
	 * @inheritDoc
	 */
	public function getClientInfo(string $name, string $password): \webnode\oauth2\Entity\Client
	{
		try
		{
			$response = $this->httpClient->request(
				'GET',
				self::PATH,
				['http_errors' => false, 'auth' => [$name, $password],]
			);
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}
		
		$data = $this->responseValidator->getValidJsonResponseData($response);

		if (empty($data['clientId']) ||
			empty($data['partnerIdentifier']) ||
			!is_array($data['redirectUris'] ?? null)
		)
		{
			throw new UnexpectedResponseException('Invalid data: '. json_encode($data));
		}

		return new \webnode\oauth2\Entity\Client(
			$data['clientId'],
			$data['partnerIdentifier'],
			$data['redirectUris'],
		);
	}
}
