<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use webnode\oauth2\Entity\Client;
use webnode\oauth2\Entity\TokenData;
use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Service\AccessInterface;
use webnode\oauth2\Service\AuthorizationInterface;
use webnode\oauth2\Service\OAuth2ApiInterface;
use webnode\oauth2\Service\RevokeInterface;
use webnode\oauth2\Service\StatusInterface;
use webnode\oauth2\Service\TokenInfoInterface;

final class WebnodeOAuth2Api implements OAuth2ApiInterface
{
	/**
	 * @internal
	 */
	public function __construct(
		private readonly AccessInterface $accessTokenService,
		private readonly AuthorizationInterface $authCodeService,
		private readonly StatusInterface $statusService,
		private readonly TokenInfoInterface $infoService,
		private readonly RevokeInterface $revokeService,
		private readonly ClientService $clientService
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function passwordGrant(
		string $username,
		string $password,
		array $scope = ['basic'],
		array $sessionData = null
	): Tokens {
		return $this->accessTokenService->passwordGrant($username, $password, $scope, $sessionData);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function authCodeGrant(string $code, array $scope = ['basic'], array $sessionData = null): Tokens
	{
		return $this->accessTokenService->authCodeGrant($code, $scope, $sessionData);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function clientCredentialsGrant(array $scope = ['basic'], array $sessionData = null): Tokens
	{
		return $this->accessTokenService->clientCredentialsGrant($scope, $sessionData);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getLinkedAuthorizationCode(string $accessToken): string
	{
		return $this->authCodeService->getLinkedAuthCode($accessToken);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function revokeTokens(Tokens $tokens, bool $revokeRefreshByAccess): void
	{
		$this->revokeService->revokeTokens($tokens, $revokeRefreshByAccess);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getTokenInfo(string $token, ?string $urlPath = null): TokenData
	{
		return $this->infoService->getTokenData($token, $urlPath);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function isOAuth2ServerOk(): bool
	{
		return $this->statusService->isOAuth2ServerOk();
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getAuthCodeForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string {
		return $this->authCodeService->getAuthCodeForUser($userIdentifier, $scope, $sessionData, $ttl);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getAuthorizationUrlForUser(
		string $userIdentifier,
		string $redirectUri,
		?string $appRedirectUri = null,
		?string $stateRedirectUri = null,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string {
		return $this->authCodeService->getAuthorizationUrlForUser(
			$userIdentifier,
			$redirectUri,
			$appRedirectUri,
			$stateRedirectUri,
			$scope,
			$sessionData,
			$ttl
		);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function getAccessForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): Tokens {
		return $this->authCodeGrant($this->getAuthCodeForUser($userIdentifier, $scope, $sessionData, $ttl), $scope);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function revokeUserAccess(string $userIdentifier): void
	{
		$this->revokeService->revokeUserAccess($userIdentifier);
	}
	public function getCurrentClientInfo(): Client
	{
		return $this->clientService->getCurrentClientInfo();
	}

	public function getClientInfoByClientId(string $clientId, string $clientSecret): Client
	{
		return $this->clientService->getClientInfo($clientId, $clientSecret);
	}

	public function getClientInfoByPartnerIdentifier(string $partnerIdentifier, string $clientSecret): Client
	{
		return $this->clientService->getClientInfo($partnerIdentifier, $clientSecret);
	}
}
