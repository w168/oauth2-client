<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use Psr\Http\Message\ResponseInterface;
use webnode\oauth2\Exception\AccessDeniedException;
use webnode\oauth2\Exception\InvalidRequestException;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;

class ResponseValidator
{
	/**
	 * @param ResponseInterface $response
	 * @return mixed[]
	 * @throws OAuth2Exception
	 */
	public function getValidJsonResponseData(ResponseInterface $response): array
	{
		$bodyStr = (string) $response->getBody();
		$body = json_decode($bodyStr, true);
		if (json_last_error() !== JSON_ERROR_NONE)
		{
			throw new UnexpectedResponseException(
				'Invalid response: ' . substr($bodyStr, 0, 100)
			);
		}
		
		if ($response->getStatusCode() === 401)
		{
			throw (new AccessDeniedException($body['message']))->setErrorIdentifier($body['error']);
		}
		elseif ($response->getStatusCode() >= 400 && $response->getStatusCode() < 500)
		{
			throw (new InvalidRequestException($body['message']))->setErrorIdentifier($body['error']);
		}
		elseif ($response->getStatusCode() !== 200)
		{
			throw new UnexpectedResponseException(
				'Invalid status code '.$response->getStatusCode().' with data: '. json_encode($body)
			);
		}
		
		return $body;
	}
	
	/**
	 * @param ResponseInterface $response
	 * @param bool $requireRefreshToken
	 * @return mixed[]
	 * @throws OAuth2Exception
	 */
	public function getValidTokenResponse(ResponseInterface $response, bool $requireRefreshToken = true): array
	{
		$body = $this->getValidJsonResponseData($response);
	
		if (empty($body['token_type']) ||
			$body['token_type'] !== 'Bearer' ||
			empty($body['expires_in']) ||
			empty($body['access_token']) ||
			($requireRefreshToken && empty($body['refresh_token']))
		)
		{
			throw new UnexpectedResponseException('Response is Not Valid Bearer Token Response');
		}
		return $body;
	}
}
