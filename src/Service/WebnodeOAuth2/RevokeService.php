<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\RevokeInterface;

final class RevokeService implements RevokeInterface
{
	private const PATH = '/revoke';
	private const REVOKE_USER_ACCESS_PATH = '/revoke_user_access';
	
	public function __construct(
		private readonly string $clientId,
		private readonly string $clientSecret,
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function revokeTokens(Tokens $tokens, bool $revokeRefreshByAccess): void
	{
		try
		{
			$data = [
				'client_id'     => $this->clientId,
				'client_secret' => $this->clientSecret,
			];
			
			if ($tokens->getAccess())
			{
				$data['access_token'] = $tokens->getAccess();
			}
			
			if ($tokens->getRefresh())
			{
				$data['refresh_token'] = $tokens->getRefresh();
			}
			
			if ($revokeRefreshByAccess)
			{
				$data['all'] = 1;
			}
			
			$response = $this->httpClient->request(
				'POST',
				self::PATH,
				['http_errors' => false, 'form_params' => $data,]
			);
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}
		
		$body = $this->responseValidator->getValidJsonResponseData($response);
		
		if (!($body['success'] ?? false))
		{
			throw new UnexpectedResponseException('Could not revoke tokens, response: ' . print_r($body, true));
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public function revokeUserAccess(string $userIdentifier): void
	{
		try
		{
			$data = [
				'client_id'     => $this->clientId,
				'client_secret' => $this->clientSecret,
				'user_identifier' => $userIdentifier
			];
			
			$response = $this->httpClient->request(
				'POST',
				self::REVOKE_USER_ACCESS_PATH,
				['http_errors' => false, 'form_params' => $data,]
			);
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}
		
		$body = $this->responseValidator->getValidJsonResponseData($response);
		
		if (!($body['success'] ?? false))
		{
			throw new UnexpectedResponseException('Could not revoke tokens, response: ' . print_r($body, true));
		}
	}
}
