<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Service\AccessInterface;

final class AccessTokenService implements AccessInterface
{
	private const PATH = '/access_token';
	
	public function __construct(
		private readonly string $clientId,
		private readonly string $clientSecret,
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function passwordGrant(
		string $username,
		string $password,
		array $scope = ['basic'],
		array $sessionData = null
	): Tokens {
		return $this->sendAndValidateRequest([
			'grant_type' => 'password',
			'username'   => $username,
			'password'   => $password,
			'scope'   => implode(' ', $scope),
		], $sessionData, true);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function authCodeGrant(string $code, array $scope = ['basic'], array $sessionData = null): Tokens
	{
		return $this->sendAndValidateRequest([
			'grant_type' => 'authorization_code',
			'code'       => $code,
			'scope'   => implode(' ', $scope),
		], $sessionData, false);
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function clientCredentialsGrant(array $scope = ['basic'], array $sessionData = null): Tokens
	{
		return $this->sendAndValidateRequest([
			'grant_type' => 'client_credentials',
			'scope'   => implode(' ', $scope),
		], $sessionData, false);
	}
	
	
	/**
	 * @param string[] $data
	 * @param mixed[]|null $sessionData
	 */
	private function sendAndValidateRequest(
		array $data,
		?array $sessionData,
		bool $requireRefreshToken = true
	): Tokens {
		try
		{
			$addedData = [];
			if ($sessionData)
			{
				$addedData['session_data'] = json_encode($sessionData);
			}
			
			$response = $this->httpClient->request('POST', self::PATH, [
				'http_errors' => false,
				'form_params'        => array_merge($data, $addedData, [
					'client_id'     => $this->clientId,
					'client_secret' => $this->clientSecret,
				]),
			]);
		}
		catch (GuzzleException $e)
		{
			// tohle by se nikdy nemělo stát, protože nahoře je http_errors = false
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}

		/** @var array{access_token: string, expires_in: int, refresh_token: ?string} $body */
		$body = $this->responseValidator->getValidTokenResponse($response, $requireRefreshToken);
		return (new Tokens())
			->setAccess($body['access_token'])
			->setExpiresIn($body['expires_in'])
			->setRefresh($body['refresh_token'] ?? null);
	}
}
