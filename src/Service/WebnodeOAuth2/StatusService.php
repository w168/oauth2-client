<?php

declare(strict_types=1);

namespace webnode\oauth2\Service\WebnodeOAuth2;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use webnode\oauth2\Exception\OAuth2Exception;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\StatusInterface;

final class StatusService implements StatusInterface
{
	private const PATH = '/status';
	
	public function __construct(
		private readonly string $clientId,
		private readonly Client $httpClient,
		private readonly ResponseValidator $responseValidator
	) {
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function isOAuth2ServerOk(): bool
	{
		try
		{
			$response = $this->httpClient->request('GET', self::PATH, [
				'http_errors' => false,
				'query' => [
					'client_id' => $this->clientId
				]
			]);
		}
		catch (GuzzleException $e)
		{
			// should not happen when http_errors = false is set
			throw new OAuth2Exception('Unexpected Guzzle exception', 0, $e);
		}
		
		$body = $this->responseValidator->getValidJsonResponseData($response);
		
		if (empty($body['ok']))
		{
			throw new UnexpectedResponseException('Response data are not valid ' . print_r($body, true));
		}
		
		return $body['ok'];
	}
}
