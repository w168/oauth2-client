<?php

namespace webnode\oauth2\Service;

use webnode\oauth2\Entity\Client;
use webnode\oauth2\Entity\TokenData;
use webnode\oauth2\Entity\Tokens;
use webnode\oauth2\Exception\OAuth2Exception;

/**
 * Interface copying our OAuth2 API
 */
interface OAuth2ApiInterface
{
	/**
	 * Get token using password Grant (both access and refresh token are acquired)
	 *
	 * @param string $username
	 * @param string $password
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function passwordGrant(
		string $username,
		string $password,
		array $scope = ['basic'],
		array $sessionData = null
	): Tokens;
	
	
	/**
	 * @param string $code
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function authCodeGrant(string $code, array $scope = ['basic'], array $sessionData = null): Tokens;
	
	
	/**
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function clientCredentialsGrant(array $scope = ['basic'], array $sessionData = null): Tokens;
	
	
	/**
	 * Returns AuthorizationCode Connected to accessToken
	 * When this AT is revoked or expired, application with access granted by this code, will lose it (logout)
	 *
	 * @param string $accessToken
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getLinkedAuthorizationCode(string $accessToken): string;
	
	
	/**
	 * @param string $token
	 * @param string|null $urlPath (override cesty)
	 * @return TokenData
	 * @throws OAuth2Exception
	 */
	public function getTokenInfo(string $token, ?string $urlPath = null): TokenData;
	
	
	/**
	 * @param Tokens $tokens
	 * @param bool $revokeRefreshByAccess
	 * @throws OAuth2Exception
	 */
	public function revokeTokens(Tokens $tokens, bool $revokeRefreshByAccess): void;
	
	
	/**
	 * @return bool
	 * @throws OAuth2Exception
	 */
	public function isOAuth2ServerOk(): bool;
	
	
	/**
	 * Returns AuthCode generating accessToken for UserId
	 *
	 * @param string $userIdentifier
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @param int|null $ttl
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getAuthCodeForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string;
	
	
	/**
	 * Returns Authorization uri for authorizing users browser with auhserver
	 * (Normaly that would be on oauth2 servers side, but in our usecase oauth2 server does not have a loginPage)
	 *
	 * @param string $userIdentifier
	 * @param string $redirectUri (where to send success/error)
	 * @param string|null $appRedirectUri (if filled, instead of redirecting to redirect uri with success,
	 * 										appRedirectUri is destination instead, with state and auth code)
	 * @param string|null $stateRedirectUri (final destination URL, can be filled with $appRedirectUri,
	 * 											to change final destination to override $redirectUri)
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @param int|null $ttl
	 * @return string
	 * @throws OAuth2Exception
	 */
	public function getAuthorizationUrlForUser(
		string $userIdentifier,
		string $redirectUri,
		?string $appRedirectUri = null,
		?string $stateRedirectUri = null,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): string;
	
	
	/**
	 * Returns accessCode for user
	 *
	 * @param string $userIdentifier
	 * @param string[] $scope
	 * @param mixed[]|null $sessionData
	 * @param int|null $ttl
	 * @return Tokens
	 * @throws OAuth2Exception
	 */
	public function getAccessForUser(
		string $userIdentifier,
		array $scope = ['basic'],
		?array $sessionData = null,
		?int $ttl = null
	): Tokens;
	
	
	/**
	 *  Revoke Access for all tokens of specific user
	 */
	public function revokeUserAccess(string $userIdentifier): void;


	/**
	 * @throws OAuth2Exception
	 */
	public function getCurrentClientInfo(): Client;

	/**
	 * @throws OAuth2Exception
	 */
	public function getClientInfoByClientId(string $clientId, string $clientSecret): Client;

	/**
	 * @throws OAuth2Exception
	 */
	public function getClientInfoByPartnerIdentifier(string $partnerIdentifier, string $clientSecret): Client;
}
