<?php

declare(strict_types=1);

namespace webnode\oauth2\Service;

use Psr\Http\Message\UriInterface;

/**
 * Fasade for aquiring URLs to redirect to
 */
interface OAuth2UrlServiceInterface
{
	/**
	 * Redirect to this URL will be send to redirectUri with authorization code in query, if brrowser is authorized
	 * Viz AuthorizationInterface::getAuthorizeBrowserUrl
	 *
	 * @param string $redirectUri
	 * @param string $state
	 * @return UriInterface
	 */
	public function getAuthorizeAppUrl(string $redirectUri, string $state = ''): UriInterface;
	
	
	/**
	 * Redirect to this URL to authorize browser with OAuth2 server.
	 * OAuth2 server will then redirect back to redirectUri
	 * Access to unauthorized application (eg. CMS) will be automaticaly granted based on browser authorization
	 *
	 * @param string $code
	 * @param string $redirectUri
	 * @param string $state
	 * @param string|null $authorizeApp
	 * @param string|null $authorityUrl (override url adresy)
	 * @param int|null $ttl
	 * @return UriInterface
	 */
	public function getAuthorizeBrowserUrl(
		string $code,
		string $redirectUri,
		string $state = '',
		?string $authorizeApp = null,
		?string $authorityUrl = null,
		?int $ttl = null
	): UriInterface;
	
	
	/**
	 * Generates single sign out URL
	 * @param string $redirectUri
	 * @return UriInterface
	 *
	 */
	public function getSSOutUrl(string $redirectUri): UriInterface;
}
