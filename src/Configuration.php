<?php

declare(strict_types=1);

namespace webnode\oauth2;

final class Configuration
{
	public function __construct(
		private readonly string $clientId,
		private readonly string $clientSecret,
		private readonly string $authBaseUri
	) {
	}
	
	
	/**
	 * @return string
	 */
	public function getClientId(): string
	{
		return $this->clientId;
	}
	
	
	/**
	 * @return string
	 */
	public function getClientSecret(): string
	{
		return $this->clientSecret;
	}
	
	
	/**
	 * @return string
	 */
	public function getAuthBaseUri(): string
	{
		return $this->authBaseUri;
	}
}
