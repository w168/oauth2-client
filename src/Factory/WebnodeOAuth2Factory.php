<?php

declare(strict_types=1);

namespace webnode\oauth2\Factory;

use GuzzleHttp\Client;
use webnode\oauth2\Configuration;
use webnode\oauth2\Service\OAuth2ApiInterface;
use webnode\oauth2\Service\OAuth2UrlServiceInterface;
use webnode\oauth2\Service\WebnodeOAuth2\AccessTokenService;
use webnode\oauth2\Service\WebnodeOAuth2\AuthorizationService;
use webnode\oauth2\Service\WebnodeOAuth2\ClientService;
use webnode\oauth2\Service\WebnodeOAuth2\ResponseValidator;
use webnode\oauth2\Service\WebnodeOAuth2\RevokeService;
use webnode\oauth2\Service\WebnodeOAuth2\StatusService;
use webnode\oauth2\Service\WebnodeOAuth2\TokenInfoService;
use webnode\oauth2\Service\WebnodeOAuth2\WebnodeOAuth2Api;
use webnode\oauth2\Service\WebnodeOAuth2\WebnodeOAuth2UrlService;

/**
 * @SuppressWarnings("CouplingBetweenObjects")
 */
final class WebnodeOAuth2Factory
{
	private Client $httpClient;
	
	
	/**
	 * @param array<string, mixed> $guzzleParams
	 */
	public function __construct(
		private readonly Configuration $configuration,
		array $guzzleParams = []
	) {
		$this->httpClient    = new Client(array_replace_recursive(
			['base_uri' => $this->configuration->getAuthBaseUri()],
			$guzzleParams
		));
	}

	/**
	 * @param string|null $clientId Overwrite value from configuration
	 * @param string|null $clientSecret Overwrite value from configuration
	 * @return OAuth2ApiInterface
	 */
	public function createApi(?string $clientId = null, ?string $clientSecret = null): OAuth2ApiInterface
	{
		$clientId          = $clientId ?? $this->configuration->getClientId();
		$clientSecret      = $clientSecret ?? $this->configuration->getClientSecret();
		$responseValidator = new ResponseValidator();
		
		return new WebnodeOAuth2Api(
			new AccessTokenService($clientId, $clientSecret, $this->httpClient, $responseValidator),
			new AuthorizationService(
				$clientId,
				$clientSecret,
				$this->configuration->getAuthBaseUri(),
				$this->httpClient,
				$responseValidator
			),
			new StatusService($clientId, $this->httpClient, $responseValidator),
			new TokenInfoService($this->httpClient, $responseValidator),
			new RevokeService($clientId, $clientSecret, $this->httpClient, $responseValidator),
			new ClientService($this->httpClient, $responseValidator, $clientId, $clientSecret),
		);
	}
	
	public function createUrlGenerator(
		?string $clientId = null,
		?string $clientSecret = null
	): OAuth2UrlServiceInterface {
		return new WebnodeOAuth2UrlService(new AuthorizationService(
			$clientId ?? $this->configuration->getClientId(),
			$clientSecret ?? $this->configuration->getClientSecret(),
			$this->configuration->getAuthBaseUri(),
			new Client(['base_uri' => $this->configuration->getAuthBaseUri()]),
			new ResponseValidator()
		));
	}
}
