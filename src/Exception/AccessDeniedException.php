<?php

declare(strict_types=1);

namespace webnode\oauth2\Exception;

/**
 * ERR 401
 */
final class AccessDeniedException extends InvalidRequestException
{
	
}
