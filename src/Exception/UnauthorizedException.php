<?php

declare(strict_types=1);

namespace webnode\oauth2\Exception;

/**
 *  JWT parse ERROR
 */
final class UnauthorizedException extends \RuntimeException
{
	
}
