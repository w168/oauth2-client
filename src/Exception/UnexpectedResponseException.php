<?php

declare(strict_types=1);

namespace webnode\oauth2\Exception;

/**
 * Unexpected server response
 */
final class UnexpectedResponseException extends OAuth2Exception
{
	
}
