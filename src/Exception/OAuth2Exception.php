<?php

namespace webnode\oauth2\Exception;

/**
 * Error from auth server
 */
class OAuth2Exception extends \RuntimeException
{
	private string $errorIdentifier;
	
	public function setErrorIdentifier(string $errorIdentifier): OAuth2Exception
	{
		$this->errorIdentifier = $errorIdentifier;
		
		return $this;
	}
	
	public function getErrorIdentifier(): string
	{
		return $this->errorIdentifier;
	}
}
