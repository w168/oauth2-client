<?php

declare(strict_types=1);

namespace webnode\oauth2\Exception;

/**
 * ERR 4**
 */
class InvalidRequestException extends OAuth2Exception
{
	
}
