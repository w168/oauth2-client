<?php

declare(strict_types=1);

namespace webnode\oauth2\Exception;

/**
 * 5xx Codes
 */
final class ServerException extends OAuth2Exception
{
	
}
