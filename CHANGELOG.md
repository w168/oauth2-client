# Changelog

## [1.3.1]
### Update
- Fix JWT usage after major Update


## [1.3.0]
### Update
- Update lcobucci/jwt to v 5.3 because of php 8.3 support
- Required php version set to 8.3
- validFromJwt throws exception if token is empty

## [1.2.0]
### Update
- Update guzzle to version 7

## [1.1.0]
### Update
- PHP version 8.1 required
- New Client endpoint

## [1.0.4]
### Update
- Set  JWT package to 1.4

## [1.0.3]
### Update
- Support php without ext-sodium

## [1.0.2]
### Update
- PHP 8.1 support

## [1.0.1]
### Update
- Packagist compliance

## [1.0.0]
### Update
- First public version

## [0.2.2]
### Update
- Added session_Data
## [0.2.1]
### Update
- Revert to Guzzle 6 (for compatibility)
## [0.2.0]
### Redone
- Redone for now OAuth2 server
