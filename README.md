# OAuth2 client
Client for webnode auth/OAuth2

This can be used as SDK by partners, co no webnode library should be used.

## Usage example
### Instantiate factory
```php
$factory = new \webnode\oauth2\Factory\WebnodeOAuth2Factory(
    new \webnode\oauth2\Configuration(
        'myClientId',
        'myClientSecret',
        'https://oauth2.webnode.com'
    )
);
```

### Generate Single Sign On Url
```php
// generate SSO Link
$oauthApiService = $factory->createApi();
$ssoUrl = $oauthApiService->getAuthorizationUrlForUser(
    $userIdentifier,// obtained from gateway-api with project info or registration
    $redirectUrl // CMS or Administration (portal) URL, both can be obtained with same request as userIdentifier 
);
// now just redirect to $ssoUrl
```

### Generate Single Sign Out Url
```php
// generate SSO Link
$urlGenerator = $factory->createUrlGenerator();
$ssOutUrl = $urlGenerator->getSSOutUrl(); // this url is universal, since it just disables access to current session
// now just redirect to $ssOutUrl
```

### Logout user from all sessions
This can be used when changing password, disabling users access for violation and so on.
```php
// generate SSO Link
$oauthApiService = $factory->createApi();
$urlGenerator->revokeUserAccess($userIdentifier);
// now just redirect to $ssOutUrl
```

