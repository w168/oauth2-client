<?php

declare(strict_types=1);

namespace webnode\oauth2\tests\unit;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use webnode\oauth2\Service\WebnodeOAuth2\AccessTokenService;
use webnode\oauth2\Service\WebnodeOAuth2\ResponseValidator;

final class AccessTokenServiceTest extends TestCase
{
	private MockObject&ResponseValidator $responseValidator;
	private MockObject&Client $httpClient;
	
	public function setUp(): void
	{
		$this->responseValidator = $this->createMock(ResponseValidator::class);
		$this->httpClient = $this->createMock(Client::class);
	}
	
	private function getTokenData(): array
	{
		return [
			'token_type' => 'Bearer',
			'expires_in' => 123,
			'access_token' => 'abc',
			'refresh_token' => 'xyz',
		];
	}
	
	/**
	 * @param array $expectedBody
	 */
	private function setClientExpectsRequestBody(array $expectedBody)
	{
		$this->httpClient->expects($this->once())->method('request')->with(
			$this->equalTo('POST'),
			$this->equalTo('/access_token'),
			$this->callback(function ($arr) use ($expectedBody) {
				$requestData = $arr['form_params'];
				foreach ($expectedBody as $key => $value)
				{
					$this->assertEquals(
						$value,
						$requestData[$key] ?? null,
						'Invalid request body param '. $key
					);
				}
				return true;
			})
		)->willReturn(new Response());
	}
	
	/**
	 * @test
	 */
	public function will_send_correct_data_for_password_grand(): void
	{
		$this->setClientExpectsRequestBody([
			'grant_type' => 'password',
			'client_id' => 'id',
			'client_secret' => 'secret',
			'username' => 'user',
			'password' => 'pwd',
		]);
		
		$this->responseValidator
			->expects($this->once())
			->method('getValidTokenResponse')
			->with($this->anything(), $this->equalTo(true))
			->willReturn($this->getTokenData());
		
		$service = new AccessTokenService('id', 'secret', $this->httpClient, $this->responseValidator);
		$this->assertEquals($this->getTokenData(), $service->passwordGrant('user', 'pwd')->toArray());
	}
	
	/**
	 * @test
	 */
	public function will_send_correct_data_for_client_grand(): void
	{
		$this->setClientExpectsRequestBody([
			'grant_type' => 'client_credentials',
			'client_id' => 'id',
			'client_secret' => 'secret',
		]);
		
		
		$this->responseValidator
			->expects($this->once())
			->method('getValidTokenResponse')
			->with($this->anything(), $this->equalTo(false))
			->willReturn($this->getTokenData());
		
		$service = new AccessTokenService('id', 'secret', $this->httpClient, $this->responseValidator);
		$this->assertEquals($this->getTokenData(), $service->clientCredentialsGrant()->toArray());
	}
	
	/**
	 * @test
	 */
	public function will_send_correct_data_for_code_grand(): void
	{
		$this->setClientExpectsRequestBody([
			'grant_type' => 'authorization_code',
			'client_id' => 'id',
			'client_secret' => 'secret',
			'code' => '123',
		]);
		
		
		$this->responseValidator
			->expects($this->once())
			->method('getValidTokenResponse')
			->with($this->anything(), $this->equalTo(false))
			->willReturn($this->getTokenData());
		
		$service = new AccessTokenService('id', 'secret', $this->httpClient, $this->responseValidator);
		$this->assertEquals($this->getTokenData(), $service->authCodeGrant('123')->toArray());
	}
}
