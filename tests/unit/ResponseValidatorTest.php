<?php

declare(strict_types=1);

namespace webnode\oauth2\tests\unit;

use GuzzleHttp\Psr7\Response;
use webnode\oauth2\Exception\AccessDeniedException;
use webnode\oauth2\Exception\InvalidRequestException;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\WebnodeOAuth2\ResponseValidator;

final class ResponseValidatorTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @test
	 */
	public function will_parse_valid_json_response()
	{
		$service = new ResponseValidator();
		$data = ['success' => true];
		$response = new Response(200, [], json_encode(['success' => true]));
		$this->assertEquals($data, $service->getValidJsonResponseData($response));
	}
	
	/**
	 * @test
	 */
	public function throws_unexpected_response_on_non_json()
	{
		$service = new ResponseValidator();
		$data = ['success' => true];
		$response = new Response(200, [], 'NO');
		$this->expectException(UnexpectedResponseException::class);
		$service->getValidJsonResponseData($response);
	}
	
	/**
	 * @test
	 */
	public function will_throw_access_denied_exception_on_401_and_valid_json()
	{
		$service = new ResponseValidator();
		$response = new Response(401, [], json_encode(['error' => '1', 'message' => '2']));
		$this->expectException(AccessDeniedException::class);
		$service->getValidJsonResponseData($response);
	}
	
	/**
	 * @test
	 */
	public function will_throw_invalid_request_exception_on_4xx_and_valid_json()
	{
		$service = new ResponseValidator();
		$response = new Response(400, [], json_encode(['error' => '1', 'message' => '2']));
		$this->expectException(InvalidRequestException::class);
		$service->getValidJsonResponseData($response);
	}
	
	/**
	 * @test
	 */
	public function will_throw_unexpected_response_on_valid_js_and_non200_non4xx_response()
	{
		$service = new ResponseValidator();
		$response = new Response(500, [], json_encode(['error' => '1', 'message' => '2']));
		$this->expectException(UnexpectedResponseException::class);
		$service->getValidJsonResponseData($response);
	}
}
