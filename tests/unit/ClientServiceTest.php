<?php

declare(strict_types=1);


namespace webnode\oauth2\tests\unit;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use webnode\oauth2\Exception\UnexpectedResponseException;
use webnode\oauth2\Service\WebnodeOAuth2\ClientService;
use webnode\oauth2\Service\WebnodeOAuth2\ResponseValidator;

final class ClientServiceTest extends TestCase
{

	private MockObject & ResponseValidator $responseValidator;
	private MockObject & Client $httpClient;

	public function setUp(): void
	{
		$this->responseValidator = $this->createMock(ResponseValidator::class);
		$this->httpClient = $this->createMock(Client::class);
	}


	/**
	 * @test
	 */
	public function it_will_send_request_with_explicit_params(): void
	{
		$response = new Response();
		$this->httpClient->expects($this->once())->method('request')->with(
			'GET',
			'/client',
			['http_errors' => false, 'auth' => ['name', 'password'],]
		)->willReturn($response);

		$this->responseValidator
			->expects($this->once())
			->method('getValidJsonResponseData')
			->with($response)
			->willReturn([
				'clientId' => 'clientId',
				'partnerIdentifier' => 'partnerIdentifier',
				'redirectUris' => ['uri1', 'uri2'],
			]);

		$service = new ClientService($this->httpClient, $this->responseValidator);
		$this->assertEquals(new \webnode\oauth2\Entity\Client(
			'clientId',
			'partnerIdentifier',
			['uri1', 'uri2']
		), $service->getClientInfo('name', 'password'));
	}

	/**
	 * @test
	 */
	public function it_will_send_request_and_catch_invalid_response_data(): void
	{
		$this->httpClient->expects($this->once())->method('request')->with(
			'GET',
			'/client',
			['http_errors' => false, 'auth' => ['name', 'password'],]
		)->willReturn(new Response());

		$this->responseValidator
			->expects($this->once())
			->method('getValidJsonResponseData')
			->with($this->anything())
			->willReturn([
			]);

		$this->expectException(UnexpectedResponseException::class);

		$service = new ClientService($this->httpClient, $this->responseValidator);
		$service->getClientInfo('name', 'password');
	}

	/**
	 * @test
	 */
	public function it_will_send_current_credentials_from_config(): void
	{
		$this->httpClient->expects($this->once())->method('request')->with(
			'GET',
			'/client',
			['http_errors' => false, 'auth' => ['name', 'password'],]
		)->willReturn(new Response());

		$this->responseValidator
			->expects($this->once())
			->method('getValidJsonResponseData')
			->with($this->anything())
			->willReturn([
				'clientId' => 'clientId',
				'partnerIdentifier' => 'partnerIdentifier',
				'redirectUris' => ['uri1', 'uri2'],
			]);

		$service = new ClientService($this->httpClient, $this->responseValidator, 'name', 'password');
		$this->assertEquals(new \webnode\oauth2\Entity\Client(
			'clientId',
			'partnerIdentifier',
			['uri1', 'uri2']
		), $service->getCurrentClientInfo());
	}
}
